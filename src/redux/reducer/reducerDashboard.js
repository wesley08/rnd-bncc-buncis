
import ActionType from '../globalActionType';

const stateDashboard = {
        tableBody :[ {
          id:"b1",
          date : '22 August 2018',
          detail: "Tendang ID Mobile",
          type: "Fave Project",
          contactPerson: "BUDI (0823-0000-9999)",
          
      },{
          id:"b2",
          date : '22 August 2018',
          detail: "Main Sponsor",
          type: "Event Patner BTC",
          contactPerson: "ALEX (0823-0000-9999)",
          
      }],
         cards: [
            {
              id: 1,
              name: "Contact Saved",
              total: 0,
              icon: "user-plus",
              color: "warning"
            },
            {
              id: 2,
              name: "New Collaboration in 2019",
              total: 0,
              icon: "building",
              color: "danger"
            }
          ],
          corporate:0
}
  
  


const reducerDashboard = (state = stateDashboard,action) => {
    if (action.type === ActionType.CHANGE_TOTAL){
        return {
          ...state,
          ...state.cards[0].total =  action.totalContact,
          ...state.cards[1].total =  action.totalCollaboration
        }
    }
    return state;
    
}

export default reducerDashboard;