import axios from 'axios';

const RootPath = 'http://localhost:3009/';

const Get = (path) =>  {
    const promise = new Promise((resolve, reject) =>{
        const GetAPIURL = RootPath.concat(path);
        axios.get(GetAPIURL)
            .then(res => {
                resolve(res.data);
            },err => {
                reject(err);
            })
    })
    return promise;
}




const getCorporateName =() => Get('corporate');
const getContact = ()=> Get('contact');
const getSearching = (e)=> Get(e);
const API ={
    getCorporateName,
    getContact,
    getSearching
}

export default API;