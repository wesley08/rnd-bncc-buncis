import React from 'react';
import { BarChart } from 'react-easy-chart';


const Bar = props => {
    return ( 
    <div className="m-3 col-xl-10 col-10 col-sm-10 col-md-10 col-lg-10 " style={{backgroundColor:"white", width:"48%", borderTop:"#17a2b8 3px solid"}}>
        <div className="m-2" style={{overflow:"auto"}}>
            <h6>{props.bar.title}</h6>
            <BarChart
            axes
            grid
            colorBars
            height={200}
            width={props.bar.width}
            data={ props.bar.data }
            margin={{top: 10, right: 10, bottom: 30, left: 100}}
            /> 
            {props.bar.button}
            <button onClick={()=> props.onClickMonth(props.bar.id)} id={props.bar.id} className="btn btn-info btn-sm m-2" style={{width: "100px", marginRight:"10px" }}
                >Month 
            </button>
            <button onClick={()=> props.onClickYear(props.bar.id)} id={props.bar.id} className="btn btn-info btn-sm m-2" style={{ width: "100px", marginRight:"10px" }}
                >Year
            </button>
        </div>
    </div> 
    );
};
 
export default Bar;