import React, { Component } from 'react';

class CollaborationAddEdit extends Component {
    state = { isChecked:true }
    clickCheckbox () {
        if (this.state.isChecked === true){
            document.getElementById("mySelect").disabled = false;
            this.setState({isChecked: !this.state.isChecked})
        }else {
            document.getElementById("mySelect").disabled = true;
            this.setState({isChecked: !this.state.isChecked})
        }
        
      }
      constructor() {
        super();
        this.clickCheckbox = this.clickCheckbox.bind(this);}
    render() { 
        return ( 
            <div>
                <div style={{display:"flex", justifyContent:"space-between"}}>
                    <div style={{display:"flex"}}>
                        <h1>
                            Collaboration <sub className="ubahColPadding">[Add New] or [Edit]</sub>
                        </h1>  
                    </div>
                    <div>
                        <button  className="btn btn-danger btn-sm m-2" style={{ width: "100px", marginRight:"10px" }}
                            >Cancel
                        </button>
                        <button  className="btn btn-success btn-sm m-2" style={{ width: "100px", marginRight:"10px" }}
                            >Save
                        </button>
                    </div>
                </div>
                <div className=" col-xl-11 col-11 col-sm-11 col-md-11 col-lg-11 m-3 p-3" style={{backgroundColor:"white", borderTop:"#17a2b8 3px solid"}}>
                    <h6>Information</h6>
                    <hr/>
                    <div className="row">
                        <div className=" col-xl-6 col-12 col-sm-12 col-md-12 col-lg-6 p-3">
                        <h6 style={{marginTop:"10px"}}>Collaboration Type</h6>
                            <select  className=" form-control"style={{width:"100%", marginTop:"10px"}}name="" id="">
                                <option></option>
                            </select>
                            <h6 style={{marginTop:"10px"}}>Coporate</h6>
                            <select className=" form-control" style={{width:"100%", marginTop:"10px"}}name="" id="">
                               <option></option>
                            </select>
                            <div className="m-2">
                                <input type="checkbox" name="" value="" onChange={this.clickCheckbox}/> Collaboration for Event
                            </div>
                            <h6 style={{marginTop:"10px"}}>Event Name</h6>
                            <select id="mySelect" className="form-control " style={{width:"100%", marginTop:"10px"}} disabled >
                               <option>sdf</option>
                            </select>
                        </div>
                        <div className=" col-xl-6 col-12 col-sm-12 col-md-12 col-lg-6 p-3">
                        <h6 style={{marginTop:"10px"}}>Date</h6>
                        <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"/></span>
                </div>
                <input type="text" class="form-control" placeholder="Enter Date" aria-label="Enter Date" aria-describedby="basic-addon1"></input>
            </div> 
                            <h6 style={{marginTop:"10px"}}>Contact Person</h6>
                            <select className=" form-control" style={{width:"100%", marginTop:"10px"}}name="" id="">
                                <option></option>
                            </select>
                            <h6 style={{marginTop:"10px"}}>Collaboration Description</h6>
                            <textarea className="p-1 form-control" type="area" style={{width:"100%"}} placeholder="Enter Address"/>
                        </div>
                    </div>
                </div>
            </div>
         );
    }
}
 
export default CollaborationAddEdit;