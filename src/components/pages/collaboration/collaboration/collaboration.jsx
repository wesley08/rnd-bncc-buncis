import React, { Component } from 'react';
import Table from '../../../common/table/table/table'
import { Link } from 'react-router-dom';
import API from '../../../../services/index';


class Collaboration extends Component {
    state = { 
        table : {
        tableHead: [
         {
             id:"1",
             path:"type",
             label : "Type"
         },
         {
             id:"2",
             path:"corporateName",
             label : "Coporate Name"
         },
         {
             id:"3",
             path:"description",
             label :"Description"
         },
         {
             id:"4",
             path:"date",
             label : "Date"
         },
         {
             id:"5",
             path:"contactperson",
             label : "ContactPerson"
         },
         {
             id:"7",
             content: user => (
                 <div>
                     <button className="btn btn-warning btn-sm mr-1"  onClick={this.handleClick} 
                     >Edit
                     </button>
                     <button className="btn btn-danger btn-sm mr-1" onClick={this.handleClick2} 
                     >Delete
                     </button>
                 </div>
             )
         },
        ],
     tableBody :[] ,
     colors: "bg-success"} }
     getAPI = () =>{
        API.getCorporateName().then(res => {
            this.setState(this.state.table.tableBody = res)
        })
      }
    render() { 
        return ( <div >
            <div style={{display:"flex", justifyContent:"space-between", alignContent:"center"}}>
                <h2 >Collaboration</h2> 
                   <Link className="designLink" to="/collaboration/collaborationAddEdit">
                   <button  className="btn btn-info btn-sm m-2" style={{  marginRight:"10px" ,fontSize:"40%"}}
                        >Add New Collaboration
                    </button>
                   </Link> 
            </div>
            <div className="row">
            <div className=" col-xl-4 col-10 col-sm-10 col-md-6 col-lg-4 m-3" style={{backgroundColor:"white", width:"35%", borderTop:"#17a2b8 3px solid", height:"400px"}}>
                <div className="m-3" style={{ display:"flex",justifyContent:"space-between"}}>
            Filters  
            <button  className="btn btn-info btn-sm "
                >Apply Filters
            </button>
            </div>
              <hr/> 
              <div className="m-2">
              <form className="m-3"  action="">
              <h6 className="mb-3">Collaboration Desc</h6>
                <input className="mb-3 form-control" type="text" style={{width:"100%"}} placeholder="Enter Description"/>
                <h6 className="mb-3">Corporate Name</h6>
                <select  className="mb-3 custom-select custom-select-sm"style={{width:"100%"}}>
                    <option></option>
                </select>
              </form>
            </div>   
                
            </div>
            <div className="col-xl-7 col-10 col-sm-10 col-md-10 col-lg-7 m-3" style={{backgroundColor:"white", width:"60%", borderTop:"#17a2b8 3px solid"}}>
            <h6 className="m-3">Collaboration Details</h6> <hr/>    
            <div className=" ubahColPadding"style={{overflow:"auto",height:"300px"}} >
            <Table users={this.state.table}/>
            </div>
            </div>  
            </div>
                
        </div>  );
    }
}
 
export default Collaboration;