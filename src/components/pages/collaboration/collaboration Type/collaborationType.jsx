import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Table from '../../../common/table/table/table';
import  Axios  from 'axios';
import Button from '../../../common/button/button';


const apiEndPoint ="https://jsonplaceholder.typicode.com/posts"
class CollaborationType extends Component {
    
    state = { 
        getID : '',
        table : {
        tableHead: [
         {
             id:"1",
             path:"title",
             label : "Collaboration Type"
         },
         {
             id:"2",
             path:"body",
             label : "Collaboration Description"
         },
         {
             id:"3",
             path:"id",
             label : "ID"
         },
         {
             id:"4",
             content: user => (
                 <div>
                     <button className="btn btn-warning btn-sm mr-1"  
                     >Edit
                     </button>
                     <button className="btn btn-danger btn-sm mr-1" onClick={()=>this.delete(user)}
                     >Delete
                     </button>
                 </div>
             )
         },
        ],
     tableBody :[] ,colors: "bg-success"} 
     , button :{
         tema : "info ",
         btnSize : "btn-sm ",
         margin: " m-2",
         label : "Add New Collaboration Type",
     }}
    
     delete = (e) => {
        console.log("asdf")
     }
     async componentDidMount() {
        const  {data : tableBody} = await Axios.get(apiEndPoint)
        this.setState(this.state.table.tableBody =tableBody)
      }
      getAPI=()=>{
        Axios.get('http://localhost:3009/contact')
        .then(res => {
            this.setState(this.state.table.tableBody = res.data)})
    }

      componentDidMount() {
          this.getAPI();
        }
      
    //  delete = async post => {
    //     // const tableBody = this.state.table.tableBody.filter(d => d.id !== tableID);
    //     // this.setState (this.state.table.tableBody = tableBody);
    //     // console.log(tableBody);
    //     await Axios.delete(apiEndPoint +'/'+post.id);
    //     const tableBody = this.state.table.tableBody.filter(p => p.id !== post.id);
    //     this.setState (this.state.table.tableBody = tableBody);
    //   }

      add = async () =>{
          const obj ={title :this.state.typed,body:'b'};
          const {data : post } = await Axios.post(apiEndPoint, obj)
          const tableBody =[post, ...this.state.table.tableBody];
          this.setState(this.state.table.tableBody =tableBody)
      }
        
          
    getID(id){
        this.setState( this.state.getID = id);
    }  

    render() { 
        return ( 
        
        <div>
            <div style={{display:"flex", justifyContent:"space-between", alignContent:"center"}}>
                <h2 color="red">Collaboration Type</h2> 
                   <Link className="designLink" to="/collaborationTypeAddEdit">
                        <Button button={this.state.button}  buttonclick={this.delete}/>
                   </Link> 
                   
            </div>
            <div className=" col-xl-10 col-10 col-sm-10 col-md-10 col-lg-10 m-3 p-3" style={{backgroundColor:"white",overflow:"auto", borderTop:"#17a2b8 3px solid"}}>
            <Table users={this.state.table}/>
            </div>
        </div> );
    }
}
 
export default CollaborationType;