import React, { Component } from 'react';
import Bar from '../common/bar/bar';
import axios from 'axios'

class CollaborationBar extends Component {
    state = {  
         bars: []
    }
    
    Month = (BarID) =>{
        if (BarID === 1){
            const data = [...this.state.bars[0].dataM]
            this.setState(this.state.bars[0].data = data) 
        }
        else if (BarID === 2){
            const data = [...this.state.bars[1].dataM]
            this.setState(this.state.bars[1].data= data) 
       }
    }
    Year = (BarID) =>{
        if ( BarID === 1){
            const data = [...this.state.bars[0].dataY]
            this.setState(this.state.bars[0].data= data)
        }
        else{
            const data = [...this.state.bars[1].dataY]
            this.setState(this.state.bars[1].data= data)
       }

    }
    getAPI = () => {
        axios.get('http://localhost:3009/collaborationBar')
        .then(res => {  
            this.setState({
                bars : res.data
        })})
    }

    componentDidMount (){
        this.getAPI()
    }
   
    render() { 
        return ( 
            <div>
                <h2 className="dashboardstyle">Collaboration Bar</h2>
                <div className="row" >
                    {this.state.bars.map(bar => (
                        <Bar use={this.state.bar} key={bar.id} onClickMonth={this.Month} onClickYear={this.Year} bar={bar}  />
                    ))}
                </div> 
            </div> );
            
    }
}
 
export default CollaborationBar;