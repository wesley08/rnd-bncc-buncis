import React, { Component } from 'react';
class CorporateAddExit extends Component {
    state = {  }
    
    render() { 
        return ( 
            <div>
                <div style={{display:"flex", justifyContent:"space-between"}}>
                    <div style={{display:"flex"}}>
                        <h1>
                            Corporate <sub className="ubahColPadding">[Add New] or [Edit]</sub>
                        </h1>  
                    </div>
                    <div>
                        <button  className="btn btn-danger btn-sm m-2" style={{ width: "100px", marginRight:"10px" }}
                            >Cancel
                        </button>
                        <button  className="btn btn-success btn-sm m-2" style={{ width: "100px", marginRight:"10px" }}
                            >Save
                        </button>
                    </div>
                </div>
                <div className="row" style={{display:"flex"}}>
                    <div className="m-3 col-xl-5 col-10 col-sm-10 col-md-10 col-lg-5 m-3 p-3" style={{backgroundColor:"white", width:"48%", borderTop:"#17a2b8 3px solid"}}>
                        <h6 className="m-1">Picture</h6>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile"/>
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                    <div className="m-3 col-xl-5 col-10 col-sm-10 col-md-10 col-lg-5 m-3 p-3" style={{backgroundColor:"white", width:"48%", borderTop:"#17a2b8 3px solid"}}>
                        <h6 className="m-1">Information</h6>
                        <hr/>
                        <form className="m-3" action="">
                            <h6>Corporeate Name</h6>
                            <input className="p-1 form-control" type="textfield" style={{width:"100%"}} placeholder="Corporate Name"/>
                            <h6 className="mt-2">Address</h6>
                            <textarea className="p-1 form-control" type="area" style={{width:"100%"}} placeholder="Enter Address"/>
                        </form>
                    </div>
                </div>       
            </div>   
        );
    }
}
 
export default CorporateAddExit;