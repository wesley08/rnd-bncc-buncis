import React, { Component } from 'react';
import CardCorporate from '../../../common/card/card Corporate/cardCorporate';
import { Link } from 'react-router-dom';
import API from '../../../../services/index';
import { connect } from 'react-redux';


class Corporate extends Component {
    state ={
        cards : []
    }
      deleteCorporate = corporateID => {
        const cards = this.state.cards.filter(d => d.id !== corporateID);
        this.setState ({cards});
      }
    //   getAPI = () =>{
    //     API.getCorporateName().then(res => {
    //         this.setState({cards : res})
    //     })
    //   }
      componentDidMount(){
        //   this.getAPI()
      }

    render() { 
        
        return ( 
         <div>
             <div style={{display:"flex", justifyContent:"space-between", alignContent:"center"}}>
                <h2 >Corporate</h2> 
                <Link className="designLink" to="/corporate/corporateAddEdit">
                    <button className="btn btn-info btn-sm m-2" style={{  marginRight:"10px" }}
                        >Add New Corporate
                    </button>
                </Link>  
            </div>
            <div className="card-deck">
                <div className="row m-2 row " style={{overflow:"auto",height:"600px"}}> 
                    {this.state.cards.map(card => (
                        <CardCorporate key={card.id} card={card} delete={this.deleteCorporate}/>
                    ))}
                </div>
            </div>
        </div> );
    }
}
 const mapStateToProps = (state)=>{
    return {
        cards : state.corporate
    }
 }

 const mapDispatchToProps = (dispatch) =>{
     return{
         
     }
 }
export default connect(mapStateToProps,mapDispatchToProps)(Corporate);